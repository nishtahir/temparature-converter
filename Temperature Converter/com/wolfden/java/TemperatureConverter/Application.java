package com.wolfden.java.TemperatureConverter;

import java.awt.FlowLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.text.DecimalFormat;

import javax.swing.ButtonGroup;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.SwingUtilities;

/**
 * 
 * @author Slay3r Mark 4
 */

public class Application extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4601083225504387020L;

	private final JButton convertButton;
	private final JRadioButton toCRadioButton;
	private final JRadioButton toFRadioButton;
	private final ButtonGroup buttonGroup;
	private final JTextField infield;
	private final FlowLayout layout;
	private static final int CELCIUS = 1;
	private static final int FAHRENHEIT = 2;
	private int cur_conversion;

	public Application() {
		super("Temperature Converter");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setSize(350, 65);
		setResizable(false);
		

		layout = new FlowLayout();
		setLayout(layout);

		infield = new JTextField(8);
		add(infield);


		// Create radio buttons
		toCRadioButton = new JRadioButton("to C");
		toFRadioButton = new JRadioButton("to F");
		add(toCRadioButton);
		add(toFRadioButton);

		convertButton = new JButton("Convert");
		add(convertButton);
		// Add relationship between buttons

		buttonGroup = new ButtonGroup();
		buttonGroup.add(toCRadioButton);
		buttonGroup.add(toFRadioButton);

		// Register event handlers for RadioButtons
		toCRadioButton.addItemListener(new RadioButtonHandler(CELCIUS));
		toFRadioButton.addItemListener(new RadioButtonHandler(FAHRENHEIT));

		// Create Button Handler to handle button events
		ButtonHandler bhandler = new ButtonHandler();
		convertButton.addActionListener(bhandler);
		
		setVisible(true);
	}

	/**
	 * @param args
	 *            the command line arguments
	 */
	public static void main(String[] args) {
		SwingUtilities.invokeLater(new Runnable() {

			@Override
			public void run() {
				new Application();
			}
		});
	}

	private class RadioButtonHandler implements ItemListener {

		int conv;

		public RadioButtonHandler(int conversion) {
			conv = conversion;

		}

		@Override
		public void itemStateChanged(ItemEvent e) {
			setCur_conversion(conv);
			System.out.println("radio button is set to " + cur_conversion);
		}
	}// end RadioButtonHandler

	private class ButtonHandler implements ActionListener {

		public ButtonHandler() {
		}

		@Override
		public void actionPerformed(ActionEvent e) {
			String inputText = getInfield();
			System.out.println("inputText is " + inputText);
			double inNum = Double.parseDouble(inputText);

			int conv;
			conv = getCur_conversion();
			double result;
			DecimalFormat newFormat = new DecimalFormat("#.##");
			double twoDecimal;
			String resultStr;
			switch (conv) {
			case 1:// convert from f to c
				result = (inNum - 32) * (5.0 / 9);
				System.out.println(result);
				// round the result to two decimal places

				twoDecimal = Double.valueOf(newFormat.format(result));
				System.out.println(twoDecimal);// for debuging
				resultStr = twoDecimal + ""; // Used little cheat to force
				// convert result to str
				setInfield(resultStr);
				break;

			case 2: // convert from c to f
				result = inNum * (9.0 / 5) + 32;
				System.out.println(result);
				twoDecimal = Double.valueOf(newFormat.format(result));
				System.out.println(twoDecimal);
				resultStr = twoDecimal + "";
				setInfield(resultStr);
			}
		}
	}// end class ButtonHandler

	// Getters and setters
	public int getCur_conversion() {
		return cur_conversion;
	}

	public void setCur_conversion(int arg) {
		cur_conversion = arg;
	}

	public String getInfield() {
		return infield.getText();
	}// end getInfield

	public void setInfield(String result) {
		infield.setText(result);
	}
}
